{
  description = "My personal st fork";

  inputs = {
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils }:
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages."${system}";

        rawPackage = pkgs.st.overrideAttrs (attrs: {
          src = ./.;
        });

        wrapper = pkgs.writeShellScript "st" ''
          # Start with tmux if no arguments
          if [ "$#" -eq "0" ]; then
            "${rawPackage}/bin/st" "${pkgs.tmux}/bin/tmux"
          else
            "${rawPackage}/bin/st" "$@"
          fi
        '';
      in
      rec {
        # `nix build`
        packages.st = pkgs.symlinkJoin {
          name = "st";
          paths = [ rawPackage ];

          nativeBuildInputs = [ pkgs.makeWrapper ];

          postBuild = ''
            rm "$out/bin/st"

            # Always start in tmux
            cp "${wrapper}" "$out/bin/st"
          '';
        };
        defaultPackage = packages.st;

        # `nix run`
        apps.st = utils.lib.mkApp {
          drv = packages.st;
        };
        defaultApp = apps.st;
      });
}
